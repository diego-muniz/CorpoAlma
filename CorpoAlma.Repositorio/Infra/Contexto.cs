﻿using CorpoAlma.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace CorpoAlma.Repositorio.Infra
{
  public class Contexto : DbContext
  {
    public Contexto() : base("dbcorpoalma")
    {

    }

    public IDbSet<Cliente> Cliente { get; set; }
    //public IDbSet<Produto> Produto { get; set; }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
      modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
      modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

    }


  }
}