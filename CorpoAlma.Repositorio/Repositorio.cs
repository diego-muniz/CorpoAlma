﻿using CorpoAlma.Repositorio.Infra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CorpoAlma.Repositorio
{
  public class Repositorio<T> : IDisposable where T : class
  {
    protected Contexto contexto { get; set; }
    public Repositorio()
    {
      contexto = new Contexto();
    }

    public void Dispose()
    {
      if (contexto != null)
      {
        contexto.Dispose();
        contexto = null;
      }
    }

    public List<T> Listar()
    {
      return contexto.Set<T>().ToList();
    }

    public void Novo(T registro)
    {
      try
      {
        contexto.Entry(registro).State = System.Data.Entity.EntityState.Added;
        contexto.SaveChanges();
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    public T Buscar(int Id)
    {
      try
      {
        return contexto.Set<T>().Find(Id);
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }

    public T Buscar(Expression<Func<T, bool>> filtro)
    {
      try
      {
        return contexto.Set<T>().Where(filtro).FirstOrDefault();
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }

    //Excluir 
    public void Excluir(T registro)
    {
      try
      {
        contexto.Entry(registro).State = System.Data.Entity.EntityState.Deleted;
        contexto.SaveChanges();
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }

    //Excluir por Id
    public void Excluir(int Id)
    {
      try
      {
        T registro = Buscar(Id);

        if (registro != null)
        {
          Excluir(registro);
        }
        else
        {
          throw new Exception("O registro informado não foi encontrado");
        }
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
  }
}