﻿(function () {
    'use strict';

    //inicializar modulos
    angular.module('corpoalma.configuration', []);
    angular.module('corpoalma.controller', []);

    angular.module('corpoalma', [
        'corpoalma.configuration',
        'corpoalma.controller',
        'ui.router',
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngSanitize',
        'ngTouch',
        'smart-table',
        'ngCookies',
        'ncy-angular-breadcrumb',
        'ui.select',
        'ui-notification',
        'ui.bootstrap',
        'angular-loading-bar',
        'ngLocale',
        'moment-picker'
    ]);
})();

