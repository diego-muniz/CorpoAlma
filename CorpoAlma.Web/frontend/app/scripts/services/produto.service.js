﻿(function () {
    'use strict';

    angular.module('corpoalma')
        .service('ProdutoService', ProdutoService);

    ProdutoService.$inject = ['$http', 'BASEPATH'];

    function ProdutoService($http, BASEPATH) {
        var vm = this;

        vm.service = {
            pesquisar: pesquisar,
            buscar: buscar,
            inserir: inserir,
            editar: editar
        };
        return vm.service;

        function pesquisar() {
            return $http.post(BASEPATH + 'Produto/Listar');
        }

        function buscar(id) {
            return $http.post(BASEPATH + 'Produto/BuscarId', {
                id: id
            });
        }

        function inserir(registro) {
            console.log(registro);
            return $http.post(BASEPATH + 'Produto/Inserir', {
                registro: registro
            });
        }

        function editar(registro) {
            return $http.post(BASEPATH + 'Produto/Editar', {
                registro: registro
            });
        }

    }
})();

