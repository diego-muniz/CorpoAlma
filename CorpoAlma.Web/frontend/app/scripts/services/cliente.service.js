﻿(function () {
    'use strict';

    angular.module('corpoalma')
        .service('ClienteService', ClienteService);

    ClienteService.$inject = ['$http', 'BASEPATH'];

    function ClienteService($http, BASEPATH) {
        var vm = this;

        vm.service = {
            pesquisar: pesquisar,
            buscar: buscar,
            inserir: inserir,
            editar: editar
        };
        return vm.service;

        function pesquisar() {
            return $http.post(BASEPATH + 'Cliente/Listar');
        }

        function buscar(id) {
            return $http.post(BASEPATH + 'Cliente/BuscarId', {
                id: id
            });
        }

        function inserir(registro) {
            console.log(registro);
            return $http.post(BASEPATH + 'Cliente/Inserir', {
                registro: registro
            });
        }

        function editar(registro) {
            return $http.post(BASEPATH + 'Cliente/Editar', {
                registro: registro
            });
        }

    }
})();

