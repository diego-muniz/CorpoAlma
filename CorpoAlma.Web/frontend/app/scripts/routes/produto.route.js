﻿(function () {
    'use strict';

    angular.module('corpoalma')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/');

            $urlRouterProvider.otherwise('/404');

            $stateProvider

                .state('produto', {
                    url: '/produto',
                    ncyBreadcrumb: {
                        label: 'Produtos'
                    },
                    redirectTo: 'produto.visualizar'
                })
                .state('produto.visualizar', {
                    url: '/visualizar',
                    templateUrl: 'pages/produto/consultar/consulta.view.html',
                    controller: 'ProdutoConsultaController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Produtos'
                    }
                })
        });
})();


