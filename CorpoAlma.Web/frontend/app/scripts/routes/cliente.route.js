﻿(function () {
    'use strict';

    angular.module('corpoalma')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/cliente');

            $urlRouterProvider.otherwise('/404');

            $stateProvider

                .state('cliente', {
                    url: '/cliente',
                    ncyBreadcrumb: {
                        label: 'Cliente'
                    },
                    redirectTo: 'cliente.visualizar'
                })
                .state('cliente.visualizar', {
                    url: '/visualizar',
                    templateUrl: 'pages/cliente/consultar/consulta.view.html',
                    controller: 'ClienteConsultaController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Clientes'
                    }
                })
                .state('cliente.cadastrar', {
                    url: '/cadastrar',
                    templateUrl: 'pages/cliente/cadastrar/cadastrar.view.html',
                    controller: 'ClienteCadastrarController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Clientes'
                    }
                })
                .state('cliente.editar', {
                    url: '/editar/:id',
                    templateUrl: 'pages/cliente/editar/editar.view.html',
                    controller: 'ClienteEditarController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Editar Cliente'
                    }
                })
        });
})();


