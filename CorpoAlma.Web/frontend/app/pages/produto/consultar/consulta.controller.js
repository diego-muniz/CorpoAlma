﻿(function () {
    'use strict';

    angular.module('corpoalma.controller')
        .controller('ProdutoConsultaController', ProdutoConsultaController);
    //Injeta Servico do cliente
    ProdutoConsultaController.$inject = ['ProdutoService'];

    function ProdutoConsultaController(ProdutoService) {

        var vm = this;

        init();

        //Limpar List e buscar nova lista
        function init() {
            vm.lista = [];
            buscar();
        }

        //Buscar o serviço injetado
        function buscar() {
            ProdutoService.pesquisar().then(function success(response) {
                vm.lista = response.data.retorno;
            }, function error(retorno) {
            });
        }
    }

})();

