﻿(function () {
    'use strict';

    angular.module('corpoalma.controller')
        .controller('ClienteCadastrarController', ClienteCadastrarController);

    ClienteCadastrarController.$inject = ['ClienteService'];

    function ClienteCadastrarController(ClienteService) {
        var vm = this;
        vm.salvar = salvar;

        function salvar() {
            console.log('teste');
            ClienteService.inserir(vm.registro).then(function (response) {
                console.log("Cadastrado com sucesso");
                window.location.href = 'http://localhost:9010/#!/cliente/visualizar';
            }), function (response) {
                console.log(response)
            }
        }

    }


})();