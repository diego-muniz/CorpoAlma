﻿(function () {
    'use strict';

    angular.module('corpoalma.controller')
        .controller('ClienteConsultaController', ClienteConsultaController);
    //Injeta Servico do cliente
    ClienteConsultaController.$inject = ['ClienteService'];

    function ClienteConsultaController(ClienteService) {

        var vm = this;

        init();

        //Limpar List e buscar nova lista
        function init() {
            vm.lista = [];
            buscar();
        }

        //Buscar o serviço injetado
        function buscar() {
            ClienteService.pesquisar().then(function success(response) {
                vm.lista = response.data.retorno;
            }, function error(retorno) {
            });
        }
    }

})();

