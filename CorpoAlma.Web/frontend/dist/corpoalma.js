(function () {
    'use strict';

    //inicializar modulos
    angular.module('corpoalma.configuration', []);
    angular.module('corpoalma.controller', []);

    angular.module('corpoalma', [
        'corpoalma.configuration',
        'corpoalma.controller',
        'ui.router',
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngSanitize',
        'ngTouch',
        'smart-table',
        'ngCookies',
        'ncy-angular-breadcrumb',
        'ui.select',
        'ui-notification',
        'ui.bootstrap',
        'angular-loading-bar',
        'ngLocale',
        'moment-picker'
    ]);
})();

;angular.module('corpoalma.configuration')
    .constant('BASEPATH', 'http://localhost/CorpoAlma/');
// constant Hospedagem

 ;(function () {
    'use strict';

    angular.module('corpoalma')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/cliente');

            $urlRouterProvider.otherwise('/404');

            $stateProvider

                .state('cliente', {
                    url: '/cliente',
                    ncyBreadcrumb: {
                        label: 'Cliente'
                    },
                    redirectTo: 'cliente.visualizar'
                })
                .state('cliente.visualizar', {
                    url: '/visualizar',
                    templateUrl: 'pages/cliente/consultar/consulta.view.html',
                    controller: 'ClienteConsultaController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Clientes'
                    }
                })
                .state('cliente.cadastrar', {
                    url: '/cadastrar',
                    templateUrl: 'pages/cliente/cadastrar/cadastrar.view.html',
                    controller: 'ClienteCadastrarController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Clientes'
                    }
                })
                .state('cliente.editar', {
                    url: '/editar/:id',
                    templateUrl: 'pages/cliente/editar/editar.view.html',
                    controller: 'ClienteEditarController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Editar Cliente'
                    }
                })
        });
})();


;(function () {
    'use strict';

    angular.module('corpoalma')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/');

            $urlRouterProvider.otherwise('/404');

            $stateProvider

                .state('produto', {
                    url: '/produto',
                    ncyBreadcrumb: {
                        label: 'Produtos'
                    },
                    redirectTo: 'produto.visualizar'
                })
                .state('produto.visualizar', {
                    url: '/visualizar',
                    templateUrl: 'pages/produto/consultar/consulta.view.html',
                    controller: 'ProdutoConsultaController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Produtos'
                    }
                })
        });
})();


;(function () {
    'use strict';

    angular.module('corpoalma')
        .service('ClienteService', ClienteService);

    ClienteService.$inject = ['$http', 'BASEPATH'];

    function ClienteService($http, BASEPATH) {
        var vm = this;

        vm.service = {
            pesquisar: pesquisar,
            buscar: buscar,
            inserir: inserir,
            editar: editar
        };
        return vm.service;

        function pesquisar() {
            return $http.post(BASEPATH + 'Cliente/Listar');
        }

        function buscar(id) {
            return $http.post(BASEPATH + 'Cliente/BuscarId', {
                id: id
            });
        }

        function inserir(registro) {
            console.log(registro);
            return $http.post(BASEPATH + 'Cliente/Inserir', {
                registro: registro
            });
        }

        function editar(registro) {
            return $http.post(BASEPATH + 'Cliente/Editar', {
                registro: registro
            });
        }

    }
})();

;(function () {
    'use strict';

    angular.module('corpoalma')
        .service('ProdutoService', ProdutoService);

    ProdutoService.$inject = ['$http', 'BASEPATH'];

    function ProdutoService($http, BASEPATH) {
        var vm = this;

        vm.service = {
            pesquisar: pesquisar,
            buscar: buscar,
            inserir: inserir,
            editar: editar
        };
        return vm.service;

        function pesquisar() {
            return $http.post(BASEPATH + 'Produto/Listar');
        }

        function buscar(id) {
            return $http.post(BASEPATH + 'Produto/BuscarId', {
                id: id
            });
        }

        function inserir(registro) {
            console.log(registro);
            return $http.post(BASEPATH + 'Produto/Inserir', {
                registro: registro
            });
        }

        function editar(registro) {
            return $http.post(BASEPATH + 'Produto/Editar', {
                registro: registro
            });
        }

    }
})();

;