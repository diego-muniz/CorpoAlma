﻿using CorpoAlma.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorpoAlma.Servico.Controllers
{
    public class ProdutoController : Controller
    {
    // GET: Produto
    private static List<Produto> PopularProduto()
    {
      List<Produto> lstProduto = new List<Produto>();

      Produto produto = new Produto
      {
        Id = 1,
        Nome = "Impulsivo",
        Marca = "Ava",
        Descricao = "Esquenta e gela",
        Preco = 2.0F
      };
      lstProduto.Add(produto);

      Produto produto2 = new Produto
      {
        Id = 2,
        Nome = "Vibra",
        Marca = "Ave",
        Descricao = "Esquenta e aquece",
        Preco = 3.0F
      };
      lstProduto.Add(produto2);

      Produto produto3 = new Produto
      {
        Id = 3,
        Nome = "Pinica",
        Marca = "Ave",
        Descricao = "Esquenta e bla",
        Preco = 4.0F
      };
      lstProduto.Add(produto3);
      return lstProduto;
    }

    public ActionResult Listar()
    {
      List<Produto> lstProduto = PopularProduto();

      return Json(new { retorno = lstProduto }, JsonRequestBehavior.AllowGet);

    }
  }
}