﻿using CorpoAlma.Model;
using CorpoAlma.Repositorio.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CorpoAlma.Servico.Controllers
{
  public class ClienteController : Controller
  {

    List<Cliente> lstCliente;
    public ClienteController()
    {
      lstCliente = new List<Cliente>();
    }

    // GET: Cliente
    private List<Cliente> PopularCliente()
    {

      Cliente c = new Cliente
      {
        Id = 1,
        Nome = "Angela",
        Email = "Angela@email.com",
        Telefone = "(61) 92294-4554",
        Cpf = "133.111.411-52",
        Cep = "72270210",
        Numero = 16,
        Logradouro = "QNQ 2 Conjunto 10",
        Bairro = "Ceilândia Norte (Ceilândia)",
        Localidade = "Brasília",
        Uf = "DF"
      };
      lstCliente.Add(c);

      Cliente c2 = new Cliente
      {
        Id = 2,
        Nome = "Maria",
        Email = "Maria@email.com",
        Telefone = "(61) 92294-4554",
        Cpf = "133.441.422-52",
        Cep = "72270210",
        Numero = 10,
        Logradouro = "QNQ 2 Conjunto 10",
        Bairro = "Ceilândia Norte (Ceilândia)",
        Localidade = "Brasília",
        Uf = "DF"
      };
      lstCliente.Add(c2);

      Cliente c3 = new Cliente
      {
        Id = 3,
        Nome = "Josie",
        Email = "Josie@email.com",
        Telefone = "(61) 92294-4554",
        Cpf = "144.111.422-52",
        Cep = "72270210",
        Numero = 12,
        Logradouro = "QNQ 2 Conjunto 10",
        Bairro = "Ceilândia Norte (Ceilândia)",
        Localidade = "Brasília",
        Uf = "DF"
      };
      lstCliente.Add(c3);
      return lstCliente;
    }

    public ActionResult Listar()
    {
      //List<Cliente> lstCliente = PopularCliente();
      using (ClienteRepositorio cliente = new ClienteRepositorio())
      {
        return Json(new { retorno = cliente.Listar() }, JsonRequestBehavior.AllowGet);

      }

    }

    public ActionResult Inserir(Cliente registro)
    {
      try
      {
        using (ClienteRepositorio cliente = new ClienteRepositorio())
        {
          //cliente.Novo(registro);
          lstCliente.Add(registro);


        }

        Response.StatusCode = (int)HttpStatusCode.OK;
        return Json(new { resultado = $"Usúario {registro.Nome} cadastrado com sucesso!" });
      }
      catch (Exception ex)
      {
        Response.StatusCode = (int)HttpStatusCode.BadRequest;
        return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
        throw;
      }

    }

  }
}