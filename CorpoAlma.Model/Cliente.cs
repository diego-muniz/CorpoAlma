﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorpoAlma.Model
{
  public class Cliente
  {

    public int Id { get; set; }
    public string Nome { get; set; }
    public string Telefone { get; set; }
    public string Email { get; set; }
    public string Cpf { get; set; }
    public string Cep { get; set; }
    public int Numero { get; set; }
    public string Logradouro { get; set; }
    public string Bairro { get; set; }
    public string Localidade { get; set; }
    public string Uf { get; set; }
  }
}